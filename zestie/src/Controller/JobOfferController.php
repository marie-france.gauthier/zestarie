<?php

namespace App\Controller;

use App\Entity\JobOffer;
use App\Entity\Candidate;
use App\Form\JobOfferType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/jobs")
 */
class JobOfferController extends AbstractController
{
    /**
     * @Route("/", name="jobs", methods="GET")
     */
    public function index(): Response
    {
        $jobOffers = $this->getDoctrine()
            ->getRepository(JobOffer::class)
            ->findAll();

        return $this->render('job_offer/index.html.twig', ['job_offers' => $jobOffers]);
    }

    /**
     * @Route("/{title}/{id}", name="jobShow", methods="GET")
     */
    public function show(JobOffer $jobOffer , JobOffer $jobPrevious): Response
    {
        //le job que l'on consulte
        $job = $jobOffer;
        $jobPrevious = $jobOffer;

        //récupèrer tout les Job
        $jobOffersAll = $this->getDoctrine()
        ->getRepository(JobOffer::class)
        // on récupère le l'objet du job suivant
        ->findAll();
        $jobTotal = count($jobOffersAll);

            
        //Par défaut le job suivant et précedent sont égaux au job qu'on consulte
        $jobOffersAllPrevious =$jobOffer;
        $jobOffersAllNext =$jobOffer;
        
        //la page suivante
        //Si le job suivant 
        if ($jobOffer->getId() != count($jobOffersAll)) {
            
            //récupèrer tout les Job
            $jobOffersAllNext = $this->getDoctrine()
            ->getRepository(JobOffer::class)
                // on récupère le l'objet du job suivant
            ->findOneById($jobOffer->getId() +1);
            
            // on ecrase le job en cours avec la variable du job suivant
            $jobOffer = $jobOffersAllNext ;
            
        }
        if ($jobPrevious->getId() > 1)
        {
    
        //récupèrer tout les Job
        $jobOffersAllPrevious = $this->getDoctrine()
        ->getRepository(JobOffer::class)
        ->findOneById($jobPrevious->getId() - 1);

        // on ecrase le job en cours avec la variable du job précedent
        $jobPrevious = $jobOffersAllPrevious;

        }

        //le apply 
        $candidate = $this->getUser();
        //Vérifier si le candidat à déjà candidaté
        $allApply = $jobOffer->getCandidate()->toArray();
        // dd($apply->getCandidate()->toArray()[0]->getId());
        $applicationSuccess = false;
        foreach ($allApply as $apply) {
            if ($apply->getId() === $candidate->getId()) {
                $applicationSuccess = true;
            } else {
                $applicationSuccess = false;
            }
        }

        return $this->render('job_offer/show.html.twig', [
            'joboffer' => $job, 
            'applicationSuccess' => $applicationSuccess,
            'jobOffersAllPrevious' => $jobOffersAllPrevious,
            'jobOffersAllNext' => $jobOffersAllNext,
            'jobTotal' => $jobTotal,
        ]);

    } 

    /**
     * @Route("/apply/{id}", name="apply", methods="GET")
     */
    public function apply(JobOffer $jobOffer): Response
    {
        if($this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY')) {
            //on récupère l'utilisateur connecté 
            $candidate = $this->getUser(); // on ajoute un client au job
            $apply = $jobOffer->addCandidate($candidate);

            //on l'enregistre dans la base donnée
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($apply);
            $entityManager->flush();

            return $this->redirectToRoute('jobShow', ['id' => $jobOffer->getId()]);
        }
        

        return $this->redirectToRoute('jobShow', ['id' => $jobOffer->getId()]);
    }

    
}