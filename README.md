Deuxième projet (projet back-end) de la formation développeur web de Simplon réalisé avec Estelle Grange


Réalisation d'un site de cabinet de recruetment spécialisé dans le luxe.

Le site à été réalisé avec Symfony 4 et MySQL avec des templates HTML professionnelles.

MySQL workbench a été utilisé pour la réalisation des diagrammes et l'utilisation de la bdd.

http://zestarie.projetsymfony.tp.simplon-roanne.com/
